# Group project

the final product produced at the end of my group project. 
The code was produced over a week by 3 people including myself (Jacob Jones), Clark Senor and Karol Vince.
The code is not the most optimal in design but we had to follow a specification we had designed with little prior knowledge 
a few weeks earlier and so the code uses both source code and FXML files for the menu use.
as stated in the report we would likely change many things should we redo the project but it was a good learning experience and gave
insight into the flaws of different development methods.

JavaFx's must be installed for the program to run